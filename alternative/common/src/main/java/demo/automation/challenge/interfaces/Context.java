package demo.automation.challenge.interfaces;

public interface Context {
    void init();
    void destroy();
}
