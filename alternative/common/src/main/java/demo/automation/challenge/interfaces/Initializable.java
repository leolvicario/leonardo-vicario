package demo.automation.challenge.interfaces;

public interface Initializable {
    void initialize();
}
