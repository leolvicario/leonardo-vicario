###Alternative solution

This projects is an alternative solution for the challenge that was presented. It reflects more on what I am used to work with but it will no developed the whole extent of the tests defined for this challenge. 
It is supposed to be taken as a POC and will only contain one test case.<br/>
This solution consists on a Maven project, written in Java. In order to run it, both Maven and Java need to be installed and defined in the system's path.<br/>
<br/>
For scalability, the project is conformed by two modules. A basic structure is provided in the "common" module,
and the "web-ui" module addresses the need to provide a testing layer for UI, in this case, a web app.<br/>
<br/>
In the "common" module there will be dependencies to provide a basic structure, not relevant to the tests to be developed by themselves.
What can be found here are the dependencies that web testing, web services testing, mobile testing, and so many others could share. Our test runner "TestNG"
will be implemented in a certain level just to initiate our reporting tool by choice, "Extent Reports" which will provide
html files for each executed test. Then there are some other libraries to help in the set up processes for the suites to be run, such as "Lombok" (a plugin is required to be installed in the IDE by choice)
to get rid of all boiler plate code that makes our classes so big, "Jackson" to bind files to java classes and their attributes, useful to externalize configuration. And,
some other minor libraries for logging which are required is some extent.<br/>
So to summarize, this module will have an abstract test runner in which we will only create our html reports to display after each test run, 
the Assertion class created here acts as a wrapper to log both failing and passing assertions.
We also define here some utilities that might come handy for ensure certain structure across all possible modules, and some utilities too, to
make better usage of our local resources, this is the case of the Lazy class that you will see later.<br/>
<br/>
The "web-ui" module gets more specific to what we are going to test, in this case, and as mentioned before, a web application.
Here we will have a dependency to the "common" module to get that reporting tool and Selenium related dependencies such as the java-client, server and bonigarcia (to manage webdrivers).<br/>
In this module we implemented a page object pattern in which a Java class represents a screen from the UI, but it was taken a step further.
To avoid extensive classes, a "Component" structure was implemented, in which we split a page into reusable parts across the entire platform (if present of course). By doing this we also make our locators
smaller due to the necessity of providing a root element or container to each component, so the webdriver will start looking from that point.
Once again, to make performance better, this components will be lazily instantiated if tests needs them though a Supplier.<br/>
<br/>

###Execution
For running the tests there are several approaches to be done:
1. Run test individually by clicking the "Play" button next to the test
2. Run all the tests from a test class by clicking the "Play" button next to the class
3. Run each test suite by right clicking an ".xml" file and selecting "Run"
4. Through maven commands this can be done by passing the "suiteXmlFile" variable with 
   the desired value or by passing the "groups" variable with the desired value
   
####There are two suites only since it's a POC, one is a suite for a feature, in this case login and the other is supposed to be the full regression. If more tests were to be developed with more features and more suites, all those suites should be added here too.