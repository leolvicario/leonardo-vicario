package demo.automation.challenge.test.context;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import demo.automation.challenge.interfaces.Logging;
import demo.automation.challenge.test.config.Settings;
import lombok.SneakyThrows;
import org.openqa.grid.internal.utils.configuration.StandaloneConfiguration;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.server.SeleniumServer;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.LogManager;

import static java.lang.String.format;

public enum WebContext implements Logging {

    INSTANCE;

    private static final ThreadLocal<WebDriver> DRIVERS_PER_THREAD = new ThreadLocal<>();
    private static final ThreadLocal<SeleniumServer> SERVER_PER_THREAD = new ThreadLocal<>();
    private static final String PATH_TO_SETTINGS = "src/test/resources/settings.yml";
    private final Settings settings;

    WebContext() {
        getLogger().info("***** Initializing Web configuration *****");
        settings = readSettings();
    }

    public WebDriver getDriver() {
        return DRIVERS_PER_THREAD.get();
    }

    public SeleniumServer getServer() {
        return SERVER_PER_THREAD.get();
    }

    public void createServer() {
        stopServer();

        StandaloneConfiguration configuration = new StandaloneConfiguration();

        SeleniumServer server = new SeleniumServer(configuration);

        // Turn off verbose logging from Selenium Server...(default is ON)
        ((Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)).setLevel(Level.OFF);
        LogManager.getLogManager().getLogger("").setLevel(java.util.logging.Level.OFF);

        // Register a hook in the JVM to shut down the Selenium server cleanly before terminating
        Runtime.getRuntime().addShutdownHook(new Thread(this::stopServer));

        server.boot();

        SERVER_PER_THREAD.set(server);
    }

    @SneakyThrows
    public WebDriver init(Browser browser) {
        terminateDriver(); // Just in case we have an existing driver running in the same thread

        URL url = new URL(System.getProperty("SELENIUM_URL", "http://127.0.0.1:4444/wd/hub"));
        browser.initialize();

        Capabilities capabilities = browser.getCapabilities().merge(browser.getCapabilities());
        WebDriver driver = new RemoteWebDriver(url,capabilities);

        DRIVERS_PER_THREAD.set(driver);
        driver.get(settings.getBaseUrl());
        return driver;
    }

    public void stopServer() {
        SeleniumServer server = getServer();
        if (server != null) {
            server.stop();
        }
        SERVER_PER_THREAD.remove();
    }

    public void terminateDriver() {
        WebDriver driver = getDriver();
        if (driver != null) {
            driver.quit();
        }
        DRIVERS_PER_THREAD.remove();

    }

    private Settings readSettings() {
        Settings settings = null;
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        File file = new File(PATH_TO_SETTINGS);
        getLogger().info(format("Reading from settings file %s", file.toPath().toString()));
        try {
            settings = om.readValue(file, Settings.class);
        } catch (IOException e) {
            getLogger().info(format("Unable to extract settings from file, error -> %s", e.getMessage()));
        }
        return settings;
    }
}
