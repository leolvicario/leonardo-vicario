package demo.automation.challenge.ui;

import demo.automation.challenge.interfaces.Logging;
import lombok.SneakyThrows;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.NoSuchElementException;

import static demo.automation.challenge.reporter.Reporter.REPORTER;
import static demo.automation.challenge.test.context.WebContext.INSTANCE;
import static java.lang.Thread.sleep;
import static java.time.Duration.of;
import static java.time.Duration.ofSeconds;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public abstract class Operations implements Logging {

    protected WebDriver getDriver() {
        return INSTANCE.getDriver();
    }

    protected Operations click(WebElement element) {
        waitFor(elementToBeClickable(element));
        element.click();
        return this;
    }

    protected Operations type(WebElement element, String text) {
        waitFor(elementToBeClickable(element));
        element.clear();
        element.sendKeys(text);
        return this;
    }

    protected String getText(WebElement element) {
        waitFor(visibilityOf(element));
        return element.getText();
    }

    public String getURL() {
        REPORTER.INFO("Getting browser's URL.");
        return getDriver().getCurrentUrl();
    }

    public boolean isVisible(WebElement element) {
        try {
            waitFor(visibilityOf(element));
            return element.isDisplayed();
        }
        catch (Exception e) {
            return false;
        }
    }

    protected void scrollToElement(WebElement element) {
        REPORTER.INFO("Scrolling to element");
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    protected <T> void waitFor(ExpectedCondition<T> condition) {
        waitFor(condition, 10);
    }

    protected <T> void waitFor(ExpectedCondition<T> condition, Integer waitTime) {
        new FluentWait<>(getDriver())
                .withTimeout(ofSeconds(waitTime))
                .pollingEvery(of(1, SECONDS))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .until(condition);
    }

    @SneakyThrows
    protected void waitForLoading() {
        sleep(2000);
        REPORTER.INFO("Waiting for page to load");
        new WebDriverWait(getDriver(),  20)
                .until(driver -> ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete"));
    }
}
