package demo.automation.challenge.test.base;

import demo.automation.challenge.test.AbstractTest;
import demo.automation.challenge.test.context.WebContext;
import lombok.SneakyThrows;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

import static demo.automation.challenge.test.context.Browser.CHROME;
import static demo.automation.challenge.test.context.WebContext.INSTANCE;

public abstract class AbstractWebTest extends AbstractTest {

    @SneakyThrows
    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method method) {
        super.beforeMethod(method);
        INSTANCE.createServer();
        INSTANCE.init(CHROME);
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) {
        super.afterMethod(result);
        INSTANCE.terminateDriver();
        INSTANCE.stopServer();
    }
}
