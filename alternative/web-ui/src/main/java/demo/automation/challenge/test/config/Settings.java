package demo.automation.challenge.test.config;

import lombok.Data;

@Data
public class Settings {

    private String baseUrl;
}
