package demo.automation.challenge.ui.pages;

import demo.automation.challenge.ui.Operations;
import lombok.SneakyThrows;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractPage extends Operations {

    protected AbstractPage() {
        PageFactory.initElements(getDriver(), this);
    }

    @SneakyThrows
    public <T extends AbstractPage> T refresh(Class<T> page) {
        getDriver().navigate().refresh();
        return page.getConstructor().newInstance();
    }
}
