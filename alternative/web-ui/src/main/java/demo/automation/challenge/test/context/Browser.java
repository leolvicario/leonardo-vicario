package demo.automation.challenge.test.context;

import demo.automation.challenge.interfaces.Initializable;
import demo.automation.challenge.interfaces.Logging;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.HasCapabilities;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;

public enum Browser implements Initializable, HasCapabilities, Logging {

    CHROME {
        @Override
        public void initialize() {
            INITIALIZED.computeIfAbsent(ordinal(), n -> {
                WebDriverManager.chromedriver().setup();
                return true;
            });
        }

        @Override
        public Capabilities getCapabilities() {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-maximized");
            options.addArguments("--ignore-ssl-errors=yes", "--ignore-certificate-errors");
            return options;
        }

    };

    private static final Map<Integer, Boolean> INITIALIZED = new ConcurrentHashMap<>();

    Browser() {
        getLogger().info(format("Initializing configuration for [%s]", name()));
    }
}
