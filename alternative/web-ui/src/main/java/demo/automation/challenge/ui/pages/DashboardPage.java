package demo.automation.challenge.ui.pages;

import demo.automation.challenge.ui.components.Toolbar;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.function.Supplier;

import static demo.automation.challenge.utilities.Lazy.lazily;

public class DashboardPage extends AbstractPage {

    @FindBy(className = "header")
    private WebElement toolbarContainer;

    private final Supplier<Toolbar> toolbar = lazily(() -> new Toolbar(toolbarContainer));

    public DashboardPage() {
        waitForLoading();
    }

    public Toolbar toolbar() {
        return toolbar.get();
    }
}
