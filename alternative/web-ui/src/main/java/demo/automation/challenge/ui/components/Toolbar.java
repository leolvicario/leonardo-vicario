package demo.automation.challenge.ui.components;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class Toolbar extends AbstractComponent {

    @FindBy(css = ".topbaritem--profile span div.text-ellipsis")
    private WebElement fullNameLabel;

    public Toolbar(WebElement container) {
        super(container);
    }

    public String getFullName() {
        return getText(fullNameLabel);
    }
}
