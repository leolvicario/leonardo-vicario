package demo.automation.challenge.ui.components;

import demo.automation.challenge.ui.Operations;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

public abstract class AbstractComponent extends Operations {

    protected WebElement container;

    protected AbstractComponent(WebElement container) {
        PageFactory.initElements(new DefaultElementLocatorFactory(container), this);
        this.container = container;
    }
}
