package demo.automation.challenge.ui.pages;

import lombok.SneakyThrows;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static demo.automation.challenge.reporter.Reporter.REPORTER;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class SignInPage extends AbstractPage {

    @FindBy(id = "login-email")
    private WebElement usernameInput;

    @FindBy(id = "login-password")
    private WebElement passwordInput;

    @FindBy(css = "[uitestid='login-sign-in-button']")
    private WebElement signInButton;

    public SignInPage() {
        getDriver().get(getDriver().getCurrentUrl() + "login");
        waitFor(visibilityOfAllElements(asList(usernameInput, passwordInput, signInButton)));
    }

    public SignInPage enterEmail(String value) {
        type(usernameInput, value);
        REPORTER.INFO(format("Email entered: '%s'", value));
        return this;
    }

    public SignInPage enterPassword(String value) {
        type(passwordInput, value);
        REPORTER.INFO(format("Password entered: '%s'", value));
        return this;
    }

    @SneakyThrows
    public <T extends AbstractPage> T signIn(Class<T> page) {
        click(signInButton);
        REPORTER.INFO("Sign in button pressed");
        return page.getConstructor().newInstance();
    }
}
