package demo.automation.challenge;

import demo.automation.challenge.test.base.AbstractWebTest;
import demo.automation.challenge.ui.pages.DashboardPage;
import demo.automation.challenge.ui.pages.SignInPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

import static demo.automation.challenge.utilities.Assertion.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SignInTest extends AbstractWebTest {

    SignInPage signIn;

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod(Method m) {
        super.beforeMethod(m);
        signIn = new SignInPage();
    }

    @Test(testName = "Login - Success", groups = {"login"}, description = "User logs into the application with success and it is presented with his dashboard")
    public void loginWithSuccess() {

        DashboardPage dashboard = signIn
                .enterEmail("r4nd0mtester@fakedomain.com")
                .enterPassword("P455w0rd!")
                .signIn(DashboardPage.class);

        String fullName = dashboard.toolbar().getFullName();
        assertThat("Full name is as expected", fullName, equalTo("R4nd0m T35t3r"));
    }
}
