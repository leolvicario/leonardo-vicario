export class ForgotPassword {

  enterEmail(value) {
    cy.get('#email').type(value)
    return this
  }

  send() {
    cy.get('[type=\'submit\']').click()
    return this
  }

  validateMessage(value) {
    cy.get('#form-register .alert').contains(value)
    return this
  }
}
