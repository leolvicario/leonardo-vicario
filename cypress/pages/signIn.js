import {ForgotPassword} from './forgotPassword'
import {SignUp} from './signUp'

export class SignIn {

  enterEmail(value) {
    cy.get('.form-group-email').type(value)
    return this
  }

  enterPassword(value) {
    cy.get('.form-group-password').type(value)
    return this
  }

  signIn() {
    cy.get('[uitestid="login-sign-in-button"]').click()
    return this
  }

  forgotPassword() {
    cy.get('.login-forgot-password').click()
    return new ForgotPassword()
  }

  googleSignIn() {
    cy.xpath('//span[contains(text(),  \'Google\')]').click()
    cy.url().should('not.have.text', '/login') // cypress not allowing domain redirection, ignoring security options in false
  }

  register() {
    cy.get('[href=\'/signup\']').click()
    return new SignUp()
  }

  isPageVisible(visibility) {
    if (visibility) {
      cy.get('#login-email').should('be.visible')
    }  else {
      cy.get('#login-email').should('not.exist')
    }
    return this
  }

  validateBadEmailMessage(value) {
    cy.get('.form-group-email').contains(value)
    return this
  }

  validateBadPasswordMessage(value) {
    cy.get('.form-group-password').contains(value)
    return this
  }

}
