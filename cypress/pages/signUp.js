import {NewAccount} from './newAccount'
import {SignIn} from './signIn'

export class SignUp {

  enterFullName(value) {
    cy.get('#signup-name').type(value)
    return this
  }

  enterPhone(value) {
    cy.get('#signup-phone').type(value)
    return this
  }

  enterEmail(value) {
    cy.get('#signup-email').type(value)
    return this
  }

  enterPassword(value) {
    cy.get('#signup-password').type(value)
    return this
  }

  checkReCaptcha() {
    cy.clickRecaptcha()
    cy.wait(5000)
    return this
  }

  startFreeTrial() {
    cy.get('#button-signup').click()
    return new NewAccount()
  }

  signIn() {
    cy.get('.signup-form-header-sign-in a').click()
    return new SignIn()
  }

  anyErrorVisible() {
    cy.get('.signup-validation-failed').should('be.visible')
  }

  validateErrorMessage(value) {
    cy.xpath('//*[contains(text(), ' + value + ')]').should('be.visible')
    return this
  }

  doesUrlContains(value) {
    cy.url().should('include',  value)
    return this
  }

  isPageVisible(visible) {
    if (visible) {
      cy.get('#button-signup').should('be.visible')
    } else {
      cy.get('#button-signup').should('not.exist')
    }
    return this
  }

}
