import {SignIn} from "../pages/signIn"

const environmentUrl = Cypress.env('QA')

describe('Sign in', () => {

  var signIn
  var credentials

  before(() => {
    credentials = cy.fixture('../fixtures/credentials.json').then((json) => {
      credentials = json
    })
  })

  beforeEach(() => {
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.visit(environmentUrl + '/login')

    signIn = new SignIn()

  })

  it('with success', () => {
    signIn
      .enterEmail(credentials.username)
      .enterPassword(credentials.password)
      .signIn()
      .isPageVisible(false)
  })

  it('with invalid password', () => {
    signIn
      .enterEmail(credentials.username)
      .enterPassword('B4dP4ss!')
      .signIn()
      .validateBadPasswordMessage('Invalid credentials. Try again or click recover password')
  })

  it('with invalid email', () => {
    signIn
      .enterEmail('notAnEmail')
      .enterPassword(credentials.password)
      .signIn()
      .validateBadEmailMessage('Invalid email address')
  })

  it('with missing password', () => {
    signIn
      .signIn()
      .enterEmail(credentials.username)
      .isPageVisible(true)
  })

  it('with missing username', () => {
    signIn
      .signIn()
      .enterPassword(credentials.password)
      .isPageVisible(true)
  })

  it('forgot password', () => {
    signIn
      .forgotPassword()
      .enterEmail(credentials.anotherEmail)
      .send()
      .validateMessage('Recovery mail sent!')
  })

  it.only('register', () => {
    signIn
      .register()
      .isPageVisible(true)
      .doesUrlContains('/signup')
  })

})
