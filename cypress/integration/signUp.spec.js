import {SignUp} from "../pages/signUp"

const environmentUrl = Cypress.env('QA')

describe('Sign up', () => {

    var signUp

  beforeEach(() => {
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.visit(environmentUrl + '/signup')

    signUp = new SignUp()

  })

  it('with success', () => {
      signUp
        .enterFullName('Notunique Fullname')
        .enterPhone('+34666555444')
        .enterEmail('test.' + new Date().getTime() + '@fakeDomain.com')
        .enterPassword('P455w0rd!')
        .checkReCaptcha()
        .startFreeTrial()
        .doesUrlContains('/accounts/new')
  })

  it('with missing full name', () => {
    signUp
      .enterPhone('+34666555444')
      .enterEmail('test.' + new Date().getTime() + '@fakeDomain.com')
      .enterPassword('P455w0rd!')
      .checkReCaptcha()
      .startFreeTrial()

    signUp
      .validateErrorMessage('\'Required field\'')
  })

  it('with missing phone number', () => {
    signUp
      .enterFullName('Notunique Fullname')
      .enterEmail('test.' + new Date().getTime() + '@fakeDomain.com')
      .enterPassword('P455w0rd!')
      .checkReCaptcha()
      .startFreeTrial()

    signUp
      .validateErrorMessage('\'Required field\'')
  })

  it('with missing email', () => {
    signUp
      .enterFullName('Notunique Fullname')
      .enterPhone('+34666555444')
      .enterPassword('P455w0rd!')
      .checkReCaptcha()
      .startFreeTrial()

    signUp
      .validateErrorMessage('\'Required field\'')
  })

  it('with missing password', () => {
    signUp
      .enterFullName('Notunique Fullname')
      .enterPhone('+34666555444')
      .enterEmail('test.' + new Date().getTime() + '@fakeDomain.com')
      .checkReCaptcha()
      .validateErrorMessage('\'Required field\'')

    signUp
      .validateErrorMessage('\'Required field\'')
  })

  it('without reCaptcha', () => {
    signUp
      .enterFullName('Notunique Fullname')
      .enterPhone('+34666555444')
      .enterEmail('test.' + new Date().getTime() + '@fakeDomain.com')
      .enterPassword('P455w0rd!')
      .startFreeTrial()

    signUp
      .validateErrorMessage('\'Please confirm you are a human\'')
  })

  it('with wrong phone format', () => {
    signUp
      .enterFullName('Notunique Fullname')
      .enterPhone('0000')
      .enterEmail('test.' + new Date().getTime() + '@fakeDomain.com')
      .enterPassword('P455w0rd!')
      .checkReCaptcha()
      .anyErrorVisible()
  })

  it('with wrong email format', () => {
    signUp
      .enterFullName('Notunique Fullname')
      .enterPhone('+34666555444')
      .enterEmail('test.' + new Date().getTime() + '@fakeDomain')
      .enterPassword('P455w0rd!')
      .checkReCaptcha()
      .validateErrorMessage('\'Invalid email address\'')
  })

  it('with wrong short password', () => {
    signUp
      .enterFullName('Notunique Fullname')
      .enterPhone('+34666555444')
      .enterEmail('test.' + new Date().getTime() + '@fakeDomain.com')
      .enterPassword('simple')
      .checkReCaptcha()
      .anyErrorVisible()
  })

  it('sign in', () => {
    signUp
      .signIn()
      .isPageVisible(true)
  })

})
