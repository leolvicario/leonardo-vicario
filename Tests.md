## Test Plan

### Sign up
1. Happy path
2. Google (Not done due to redirection handle)
3. Missing fields (repeated case)
4. Wrong format (email)
5. Wrong format (phone)
6. Short password
7. reCaptcha Missing
8. No Newsletter
9. Terms and conditions
10. Login (redirection)

### Sign In
1. Happy path
2. Google (Not done due to redirection handle)
3. Missing username
4. Missing password
5. Wrong email
6. Wrong password
7. Forgot password
8. Register (redirection)

## Prioritization

The above mentioned test cases are meant to provide a decent coverage of the features that were asked.<br/>
If we would group them, I will set a priority based on the importance to ensure the correct behavior of the MVP, then "nice-to-have" functionality built around the MVP and finally "bonus features".<br/>
For "Sign Up" tests, I would place in the first group test 1, 3 (only email and password), 4, 7 (ensure avoiding multiple malicious attempts of getting into the platform) and 9 (due to legal purposes). The second group would have 2, 5 and 6. The final group would have the remaining from 3, 8 and 10.<br/>
For "Sign In" tests, I would place in the first group test 1, 3, 4, 5 and 6. This is almost the full extension but it guarantee not allowing any random or malicious login. The second group would have 2 and 7. The final group would only have 8, assuming that there are multiple other pages to redirect to register.<br/>

### Goal

By following the above prioritization we would ensure quick feedback on the MVP and if release candidate quality is achieved to promote the delivery.<br/>
The second group findings could be treated as bugs to be addressed in a "Hot Fix" but wouldn't postponed the delivery dates.<br/>
Finally the third group would throw bugs to be addressed ideally for the next release.<br/>
